import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		int L = in.nextInt();
		int S = in.nextInt();
		int[] Ladder = new int[2*L];
		int[] Snake = new int[2*S];
		for(int i = 0;i<2*L;i++){
			Ladder[i] = in.nextInt();
		}
		for(int i = 0; i<2*S;i++)
			Snake[i] = in.nextInt();
		
		solve(Ladder,Snake); 
		
	}

	private static void solve(int[] ladder, int[] snake) {
		// TODO Auto-generated method stub
		
		int[] takenladder = new int[ladder.length/2];
		takenladder = solve(ladder,snake,takenladder,100);
		Ladders ladders = new Ladders(); 
		for(int i = 0;i<takenladder.length;i++){
			if(takenladder[i] == 1) {
				int begin = ladder[i*2];
				int end = ladder[i*2+1];
				ladders.add(begin,end);
			}
			int begin = ladders.getbegin();
			int end = ladders.getend();
			ladders.removefirst(); 
		}
			
	}
	private static int[] solve(int[] ladder, int[] snake, int[] takenladder,
			int maxbegin) {
		// TODO Auto-generated method stub
		int ladderend = 0; int ladderbegin = 0;
		int t = -1;
		for(int i = 1;i<ladder.length;i+=2) {
			if(ladderend <ladder[i]&& ladder[i] <=maxbegin){
				ladderend = ladder[i];
				ladderbegin = ladder[i-1];
				t = i/2; 
			}
		}
		if(t == -1)
			return takenladder; 
		takenladder[t] = 1;
		return takenladder = solve(ladder,snake,takenladder,ladderbegin);
		 
	}

}
