import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int[] ratings = new int[N];
		for(int i = 0;i<ratings.length;i++)
			ratings[i] = in.nextInt(); 
		solve(ratings); 
	}

	private static void solve(int[] ratings) {
		// TODO Auto-generated method stub
		int num = 0; 
		int[] numcandies = new int[ratings.length];
		numcandies[0] = 1;  
		for(int i = 1; i<ratings.length;i++) {
			if(ratings[i] >ratings[i-1] && numcandies[i] <= numcandies[i-1])
				numcandies[i] =numcandies[i-1] +1; 
			else 
				numcandies[i] = 1; 
		}
		for(int i = ratings.length-1; i>0;i--) {
			if(ratings[i-1]>ratings[i] && numcandies[i-1] <= numcandies[i])
				numcandies[i-1] =numcandies[i] +1; 
			num +=numcandies[i];
		}
		num +=numcandies[0]; 
		System.out.println(num); 
	}

}
