import java.math.BigInteger;
import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for(int i=0;i<T;i++){
			int N = in.nextInt();
			int[] value = new int[N];
			for(int j =0;j<N;j++) {
				value[j] = in.nextInt();  
			}
		solve(value); 	
		}
		
	}

	private static void solve(int[] value) { // buy sell or do nothing 
		// TODO Auto-generated method stub
	hu(value);
	//System.out.println(solve(value,0,0,0));	
	}

	private static boolean hu(int[] value) {
		// TODO Auto-generated method stub
	BigInteger pw = BigInteger.valueOf(0);
	
	double p =0; double max = 0;
	for(int i = value.length-1;i>-1;i--) {
		double temp = value[i];
		
		if(temp>=max) {
			max = temp; 
		}
		int tt = (int) (max-temp);
		pw = pw.add(BigInteger.valueOf(tt));
		p += max-temp;
			
	}
	System.out.println(pw);
		return true;
	}

	private static int solve(int[] value, int item, int price, int numB) {
		// TODO Auto-generated method stub
		if(item == value.length-1) {
		if(numB>0)
			price += value[item]*numB;
		return price;
		}
		int max = solve(value,item+1,price-value[item],numB+1);// buy
		
		for(int i=0;i<=numB;i++) { // sell 
			int temp = Integer.MIN_VALUE;
			temp = solve(value,item+1,price+value[item]*i,numB-i);
			max = max >temp ? max:temp;
		}
		return max;
		
	}
}
