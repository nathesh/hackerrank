import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for(int i = 0;i<T;i++)
			solve(in.nextInt());
	}

	private static void solve(int N) {
		// TODO Auto-generated method stub
		int out = 1;
		if(N == 0) {
			System.out.println(out);
			return; 
		}
			
		for(int i = 1; i<=N;i++) {
			if(i%2 == 0)
				out +=1;
			else 
				out *=2; 
		}
		System.out.println(out);	
	}

}
