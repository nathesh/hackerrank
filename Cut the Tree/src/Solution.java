import java.util.ArrayList;
import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int vertices = in.nextInt();
		int[] weights = new int[vertices];
		int sum =0; 
		for(int i = 0;i<vertices;i++) {
			weights[i] = in.nextInt();
			sum +=weights[i];
		}
		int[] edgesfrm = new int[vertices-1];
		int[] edgesto = new int[vertices-1];
		int[] roots = new int[vertices];
		for(int i = 0;i<vertices-1;i++){
			edgesfrm[i] = in.nextInt();
			edgesto[i] = in.nextInt();
			roots[edgesto[i]] = 1; 
		}
		solve(vertices,weights,edgesfrm,edgesto,sum,roots);
	}

	private static void solve(int vertices, int[] weights, int[] edgesfrm,
			int[] edgesto,int sum, int[] roots) {
		// TODO Auto-generated method stub
		Vertex[] vertex = new Vertex[vertices];
		ArrayList<Vertex> root = new ArrayList<Vertex>();
		for(int i = 0; i<vertices;i++) {
			Vertex v  = new Vertex(weights[i],sum);
			vertex[i] = v;
			if(roots[i] == 0)
				 { v.isroot(); root.add(v); }
		}
		for(int i =0;i<vertices-1;i++) {
			int frm = edgesfrm[i]; 
			int to = edgesto[i];
			vertex[frm].addchild(vertex[to]);
			vertex[to].addparent(vertex[frm]);
		}
		for(Vertex V:root) {
			dosum(V);
		}
	
	}

	private static int dosum(Vertex v) {
		// TODO Auto-generated method stub
		ArrayList<Vertex> children = v.getchildren();
		if(children.size() == 0)
			return v.getw(); 
		int sum = 0;
		for(Vertex c:children)
			sum += dosum(c); 
		v.sum(sum); 
		return sum; 
	}

}
