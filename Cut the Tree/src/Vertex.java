import java.util.ArrayList;


public class Vertex {
	private int weight;
	private int totalsum;
	private int sum;
	private ArrayList<Vertex> parents;
	private ArrayList<Vertex> children; 
	private boolean root; 
	public Vertex(int w,int s) {
		// TODO Auto-generated constructor stub
		weight = w; 
		parents = new ArrayList<Vertex>(); 
		children = new ArrayList<Vertex>();
		totalsum = s;
		sum = w;
		root = false; 
	}
	public void addchild(Vertex child) {
		// TODO Auto-generated method stub
		children.add(child);
	}
	public void addparent(Vertex parent) {
		// TODO Auto-generated method stub
		parents.add(parent);
		
	}
	public void isroot() {
		// TODO Auto-generated method stub
		root = true; 
	}
	public ArrayList<Vertex> getchildren() {
		// TODO Auto-generated method stub
		return children;
	}
	public void sum(int sum) {
		// TODO Auto-generated method stub
		this.sum +=sum; 
	}
	public int getw() {
		// TODO Auto-generated method stub
		return weight;
	}

}
