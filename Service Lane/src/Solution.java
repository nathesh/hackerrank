import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int T = in.nextInt();
		int[] len = new int[N];
		for(int i = 0;i<N;i++) {
			len[i] = in.nextInt();
		}
		for(int i = 0;i<T;i++) {
			solve(len,in.nextInt(),in.nextInt());
		}
			
			
	}

	private static void solve(int[] len, int i, int j) {
		// TODO Auto-generated method stub
		int min = len[i];
		while(i<=j) {
			min = min < len[i] ? min:len[i];
			i++;
		}
		System.out.println(min);
		
	}

}
