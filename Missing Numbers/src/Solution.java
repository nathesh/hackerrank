import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;


public class Solution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if(test()) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Integer[] set1 = new Integer[n];
		for(int i = 0;i<n;i++){
			set1[i] = in.nextInt();
		}
		int m = in.nextInt();
		Integer[] set2 = new Integer[m];
		for(int i = 0;i<m;i++)
			set2[i] = in.nextInt();
		
		if(n>m) 
			solve(n,set1,m,set2);
		else 
			solve(m,set2,n,set1);
		}	
	}

	private static boolean test() {
		// TODO Auto-generated method stub
		int s = 10; int b = 13; 
		Integer[] s1 = {203, 204, 205, 206, 207, 208, 203, 204, 205, 206};
		Integer[] s2 = {203, 204, 204, 205, 206, 207, 205, 208, 203, 206, 205, 206, 204};
		//solve(b,s2,s,s1);
		return true;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void solve(int big, Integer[] setb, int small, Integer[] sets) {
		// TODO Auto-generated method stub
		//if(solve2(big,setb,small,sets))
			//System.exit(0);
		HashMap<Integer,Integer> mp = new HashMap<Integer,Integer>();
		for(int i = 0;i<small;i++) {
			if(mp.get(sets[i]) !=null)
				mp.put(sets[i], mp.get(sets[i])+1);
			else 
				mp.put(sets[i], 1);
		}
		ArrayList<Integer> diff = new ArrayList<Integer>();
		Set Set = new TreeSet();
		for(int i =0;i<big;i++) {
			int num = setb[i];
			if(mp.get(num) !=null && mp.get(num) !=0)
				mp.put(num, (mp.get(num)-1));
			else {
			diff.add(num);
			Set.add(num);
			}
		}
		Collections.sort(diff);
		Iterator i = Set.iterator();
		while(i.hasNext())
			System.out.print(i.next() + " ");
		
	}

	private static boolean solve2(int big, Integer[] setb, int small, Integer[] sets) {
		// TODO Auto-generated method stub
		Set Set = new TreeSet();
		ArrayList<Integer> setBigger = new ArrayList<Integer>();
		Collections.addAll(setBigger, setb); 
		setBigger.removeAll(Arrays.asList(sets));
		Set.addAll(setBigger);
		Iterator i = Set.iterator();
		while(i.hasNext())
			System.out.println(i.next());
		return true;
	}

}
